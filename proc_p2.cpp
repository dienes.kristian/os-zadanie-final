#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
 
int pipe_1;
char data[151];
FILE *fp = fopen("p2.txt","r");

void 
sig_handler_end(int signo){
   if(signo == SIGUSR2){
   printf("SIGUSR2 prijaty!\n");
   fclose(fp);
   }
}

 
void 
sig_handler(int signo){
    if(signo == SIGUSR1){
    memset(data,'\0',sizeof(data));
    fgets(data,151,fp);
    write(pipe_1,data,strlen(data));
    }
}

int 
main(int argc, char **argv)
{
if(argc != 1)
{
   printf("[P2]Nespravny pocet argumentov!");
}
pipe_1 = atoi(argv[0]);
signal(SIGUSR1, sig_handler);
signal(SIGUSR2, sig_handler_end);
printf("[P2]-> bezim!\n");
if(fp == NULL)
{
   printf("[P2]-> Nepodarilo sa otvorit subor p1.txt!\n");
   return -1;
}
printf("[P2]-> Otvoril som subor p1.txt!\n");
while(1){}
return 0;
}
