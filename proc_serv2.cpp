#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>


FILE * fp = fopen("serv2.txt","w");
char word[151];
int socket_udp;
int port_udp;
int i;

void 
sig_handler(int signo)
{
   fclose(fp);
   close(socket_udp);
   exit(0);
}

int 
main(int argc, char* argv[])
{
     signal(SIGINT, sig_handler);

     port_udp = atoi(argv[1]);
     struct sockaddr_in sock_addr, sock_other;
     memset((char *) &sock_addr, 0, sizeof(sock_addr));

     sock_addr.sin_family = AF_INET;
     sock_addr.sin_port = htons(port_udp);
     sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
     socket_udp=socket(AF_INET, SOCK_DGRAM, 0);

     bind(socket_udp, (struct sockaddr *)&sock_addr, sizeof(sock_addr));
     
     kill(getppid(), SIGUSR1);
    
     for(i = 0 ; i < 10 ; i++) {
          recvfrom(socket_udp, word, 151, 0, (struct sockaddr *)&sock_other, (socklen_t *)sizeof(sock_other));
	  fprintf(fp,"%s",word);
          fprintf(fp,"\n");
     }
     return 0;
}
