#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>


int pipe_1[2];     
int pipe_2[2];         
char pipe_1_write[150];
char pipe_1_read[150];
char pipe_2_write[150];
char pipe_2_read[150];
int p1_id , p2_id , pr_id , pt_id , ps_id , serv1_id , pd_id , serv2_id;
int mem_id1,mem_id2;
char P1[20];
char P2[20];
int sem_id_1 , sem_id_2;
char SEMAFOR1[20]; 
char SEMAFOR2[20];
void *memo1 = 0;
void *memo2 = 0;
char MEMO1[20];
char MEMO2[20];



void 
sig_handler(int signo)
{
if(signo == SIGUSR1){
printf("[zadanie]<- SIGUSR1 \n");
}
}

void 
sig_handler_end(int signo)
{
    kill(p1_id, SIGUSR2);
    kill(p2_id, SIGUSR2);
    kill(pt_id, SIGUSR2);
    kill(ps_id, SIGTERM);
    return;
}
 


main(int argc, char *argv[]) {
   char *port1 = argv[1];
   char *port2 = argv[2]; 
    signal(SIGUSR1, sig_handler);
    signal(SIGUSR2, sig_handler_end);
    pipe(pipe_1);
    pipe(pipe_2);

    ushort sem_1[] = {1,0};
    ushort sem_2[] = {1,0};

    sem_id_1 = semget(IPC_PRIVATE,2,0755);
    sem_id_2 = semget(IPC_PRIVATE,2,0755);

    semctl(sem_id_1, 0, SETALL, sem_1);
    semctl(sem_id_2, 0, SETALL, sem_2);
    
    
    sprintf(pipe_1_write,"%d",pipe_1[1]);  
    char * args[] = {&pipe_1_write[0], (char*) NULL}; 

     mem_id1 = shmget (IPC_PRIVATE, 151,0755 | IPC_CREAT);
      if (mem_id1 == -1) {
            printf("[zadanie]-> Nevytvorila sa zdielana pamat 1\n");
            exit(-1);
      }
     memo1 = shmat(mem_id1, NULL,0);
     
     mem_id2 = shmget (IPC_PRIVATE, 151,0755 | IPC_CREAT);
      if (mem_id2 == -1) {
            printf("[zadanie]-> Nevytvorila sa zdielana pamat 2\n");
            exit(-1);
     }
     memo2 = shmat(mem_id2, NULL,0);

     if(argc < 2)
     {
        printf("[zadanie]-> Pocet argumentov nie je sprany!\n");
        return -1;
     }
    
     p1_id = fork();  
     if(p1_id == 0){
     printf("[zadanie]-> spustam process P1...\n");  
     execve("./proc_p1",args, NULL);
     }
     sleep(2);

     p2_id = fork();
     if(p2_id == 0){
     printf("[zadanie]-> spustam process P2...\n");  
     execve("./proc_p2",args, NULL);
     }
     sleep(2);
 
     pr_id = fork();  
     if(pr_id == 0){
     sprintf(P1,"%d",p1_id);                    
     sprintf(P2,"%d",p2_id);                  
     sprintf(pipe_1_read,"%d",pipe_1[0]);                
     sprintf(pipe_2_write,"%d",pipe_2[1]); 
     printf("[zadanie]-> spustam process PR...\n");  
     execl("./proc_pr","./proc_pr", P1, P2, pipe_1_read, pipe_2_write, NULL);
     }
     sleep(2);


     pt_id = fork(); 
     if(pt_id == 0){    
     sprintf(pipe_2_read,"%d",pipe_2[0]);
     sprintf(MEMO1, "%d", mem_id1);
     sprintf(SEMAFOR1, "%d", sem_id_1);    
     printf("[zadanie]-> spustam process T...\n");  
     execl("./proc_t","./proc_t", pipe_2_read, MEMO1, SEMAFOR1, NULL);
     }
     sleep(2);

     ps_id = fork();
     if(ps_id == 0){
     sprintf(MEMO1,"%d",mem_id1);
     sprintf(MEMO2,"%d",mem_id2);
     sprintf(SEMAFOR2,"%d",sem_id_2);
     sprintf(SEMAFOR1,"%d",sem_id_1);
     printf("[zadanie]-> spustam process S...\n");
     execl("./proc_s", "./proc_s", MEMO1,SEMAFOR1,MEMO2,SEMAFOR2, (char *) NULL);
     }
     sleep(2);

     serv1_id = fork();
     if(serv1_id == 0){
     printf("[zadanie]-> spustam process serv1...\n");
     execl("./proc_serv1", "./proc_serv1", port1, port2,NULL);
     }
     sleep(2);


     serv2_id = fork();  
     if(serv2_id == 0){  
     printf("[zadanie]-> spustam process serv2...\n");    
     execl("./proc_serv2", "./proc_serv2", port2,NULL);
     }
     sleep(2); 	


     pd_id = fork(); 
     if(pd_id == 0){    
     sprintf(SEMAFOR2, "%d", sem_id_2);
     sprintf(MEMO2, "%d", mem_id2); 
     printf("[zadanie]-> spustam process D...\n");    
     execl("./proc_d","./proc_d", MEMO2, SEMAFOR2, port1,NULL);
     }
     printf("[zadnie]-> Koncim !");
    while(1){}
    return 0;
}

