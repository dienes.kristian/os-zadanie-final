#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/sem.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

FILE *fpipe;
int pipe_2,shm_1,sem_1;
char *shm,buffer[151];
struct sembuf char_sem_1;
int i;
int pipe_from_arg;

int
main(int argc,char *argv[])  {
    printf("[T]-> bezim!\n");
    kill(getppid(),SIGUSR1);
    pipe_from_arg = atoi(argv[1]);
    fpipe = fdopen(pipe_from_arg,"r");
    shm_1 = atoi(argv[2]);
    sem_1 = atoi(argv[3]);
    shm = (char *) shmat(shm_1,NULL,0);
    if(argc < 3)
     {
        printf("[T]-> Pocet argumentov nie je sprany!\n");
        return -1;
     }
    while(1)  {
        char_sem_1.sem_num = 0;
        char_sem_1.sem_op = -1;
        char_sem_1.sem_flg = 0;
        if(semop(sem_1,&char_sem_1,1)==-1)
        {
            printf("[T]-> Prepnutie semaforu S1[0] bolo neuspesne!\n");
            return -1;
        }
        printf("[T]-> Prepnutie semaforu S1[0] bolo uspesne!\n");
        fgets(buffer,151,fpipe);
        printf("[T]-> Vkladam do pamata: %s",buffer);
        sleep(1);
        for(i = 0 ; buffer[i] != 10 ; i++){
            buffer[151] = '\0';
            shm[i] = buffer[i];
            shm[151] = '\0';
        }
        char_sem_1.sem_num = 1;
        char_sem_1.sem_op = 1;
        char_sem_1.sem_flg = 0;
        if(semop(sem_1,&char_sem_1,1)==-1)
        {
            printf("[T]-> Prepnutie semaforu S1[1] bolo neuspesne!\n");
            return -1;
        }
        printf("[T]-> Prepnutie semaforu S1[1] bolo uspesne!\n");
        sleep(2);
    }
   return 0;
}
