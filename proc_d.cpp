#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include <sys/shm.h>

struct hostent *server;
struct sockaddr_in serv_addr;
struct sembuf sem_b;
  
int port;
int socket_des;
int sem_2; 
int mem_2;

int 
main(int argc, char *argv[]) {

char *data;
char buffer[151];
char localhost[] = "127.0.0.1";
    

     mem_2 = atoi(argv[1]);
     sem_2 = atoi(argv[2]);
     port = atoi(argv[3]);
     data = (char*) shmat(mem_2, NULL, 0);
     
     socket_des = socket(PF_INET, SOCK_STREAM, 0);

     server = gethostbyname(localhost);

     memset((char *) &serv_addr,0, sizeof (serv_addr));
     serv_addr.sin_family = AF_INET;
     memcpy((char *) &serv_addr.sin_addr.s_addr,(char *) server->h_addr, server->h_length);
     serv_addr.sin_port = htons(port);

     int value = connect(socket_des, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
     if(value < 0)
     {
            printf("[D]-> Spojenie TCP/IP zlyhalo!\n");
            return -1;
     }
     printf("[D]-> Spojenie OK!");
    
    if(argc < 3)
     {
        printf("[D]-> Pocet argumentov nie je sprany!\n");
        return -1;
     }
	
    while (1) {  
               memset(buffer,'\0', 151);
               strcpy(buffer, data);

                write(socket_des, buffer, 151);
		sem_b.sem_num = 0;
		sem_b.sem_op = 1;
		sem_b.sem_flg = SEM_UNDO;
		semop(sem_2, &sem_b, 1);

		sem_b.sem_num = 1;
		sem_b.sem_op = -1;
		sem_b.sem_flg = SEM_UNDO;
		semop(sem_2, &sem_b, 1);
	}
    return 0;
}
  

